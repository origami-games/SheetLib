package co.origamigames.sheet.block;

import net.minecraft.block.BlockState;
import net.minecraft.block.StairsBlock;

public class PublicStairsBlock extends StairsBlock {
    public PublicStairsBlock(BlockState baseBlockState, Settings settings) {
        super(baseBlockState, settings);
    }
}
