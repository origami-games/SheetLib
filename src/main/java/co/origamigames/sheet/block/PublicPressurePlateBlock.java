package co.origamigames.sheet.block;

import net.minecraft.block.PressurePlateBlock;

public class PublicPressurePlateBlock extends PressurePlateBlock {
    public PublicPressurePlateBlock(ActivationRule type, Settings settings) {
        super(type, settings);
    }
}
